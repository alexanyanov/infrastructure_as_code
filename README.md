<h1>Playbook of roles for transfer between hosts of users and their data, groups, packages, files sudoers</h1>
<h2>host</h2>
[donar]
donar1 ansible_host=...  - donar ip address 

[accepter]
accepter1 ansible_host=... - accepter ip address 

<h2>group_vars/all.yaml</h2>
File group_vars/all.yaml content variable "path_to_hosts_data".
This variable specifies the path where to save the migrated data on the local host.

<h2>playbook.yaml</h2>
